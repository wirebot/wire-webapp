FROM node:onbuild
RUN npm install -g grunt-cli
RUN useradd -ms /bin/bash someone
RUN chown -R someone:someone /usr/src/app
USER someone
RUN mv .git .gitx && npm install && mv .gitx .git
EXPOSE 8888
CMD ["/usr/local/bin/grunt", "--force"]
